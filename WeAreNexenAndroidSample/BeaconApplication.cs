﻿using BE.Wearenexen;
using Android.Runtime;
using System;

using System.Collections.Generic;
using Android.App;
using Android.Content;
using BE.Wearenexen.Network;
using BE.Wearenexen.Network.Response.Beacon;
using Java.Lang;
using static BE.Wearenexen.NexenProximityManager;

namespace WeAreNexenAndroidSample
{
    public class BeaconApplication : Application, INexenServiceInterface
    {
        private static NexenProximityManager _manager = Get();
         public static NexenProximityManager Manager { get { return _manager; } }

        public BeaconApplication(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }

        public override void OnCreate()
        {
            base.OnCreate();
            RegisterReceiver(new GeofenceBroadcastReceiver(), new IntentFilter());
            RegisterReceiver(new GeofenceListBroadcastReceiver(), new IntentFilter());

            Manager.ConfigureFor(this, this, new NexenProximitySetup("appuser", "banana"));
            var consumer = new NexenBeaconConsumer(Manager);

            Manager.Start();

        }

        public void OnLocationsLoaded(IList<Location> p0, APIError p1)
        {
        }

        public void OnProximityZoneNotification(BaseNexenZone p0)
        {
            NexenNotificationBuilder.ShowNotificationForProximityZone(this,
                p0,
                Class.FromType(typeof(NotificationActivity)),
                Resource.Drawable.pharos_notification_icon,
                this.ApplicationContext.Resources.GetString(Resource.String.notificationTitle),
                Resource.Color.orange);
        }

        public void OnProximityZonesLoadedFromCache(ICollection<NexenBeaconZone> p0)
        {
        }

        public void OnRequirePermissions(IRunnable p0)
        {
        }

    }
}
